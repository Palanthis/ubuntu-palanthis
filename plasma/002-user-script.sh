#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause License
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo apt install -y dolphin-plugins
