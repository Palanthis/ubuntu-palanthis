#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Apps
sudo apt install -y conky screenfetch lolcat cairo-dock cairo-dock-plug-ins
sudo apt install -y qt5-style-kvantum keepassxc samba smb4k rhythmbox ffmpegthumbs
sudo apt install -y pulseeffects unrar simplescreenrecorder adapta-gtk-theme
sudo apt install -y flameshot kid3-qt audacity grsync dkms ccache htop gnome-disk-utility

# Virtualization
sudo apt install -y qemu-kvm virt-manager

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/opentype ] || sudo mkdir /usr/share/fonts/opentype
sudo [ -d /usr/share/fonts/truetype ] || sudo mkdir /usr/share/fonts/truetype
sudo [ -d ~/.local/share/templates ] || sudo mkdir -p ~/.local/share/templates
sudo tar xzf tarballs/adobe-source-code-pro.tar.gz -C /usr/share/fonts/opentype --overwrite
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/opentype/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/truetype/ --overwrite
sudo tar xzf tarballs/buuf-icons-for-plasma.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo cp FunStuff/distributor-logo-ubuntu.svg /usr/share/icons/ --overwrite
#sudo tar xzf tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite

# Add screenfetch to .bashrc
echo 'screenfetch | lolcat' >> ~/.bashrc

echo " "
echo "All done!"
